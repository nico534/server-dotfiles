# Server dotfiles

My configs working on servers

Setup-Script for ubuntu-servers and automated symlink-creation to .config folder.

```bash
cd ~
git clone https://gitlab.com/nico534/server-dotfiles.git .dotfiles
cd .dotfiles/config/script
./setupUbuntu.sh
./createSymlinks.sh
```
In first nvim session run

`:PlugInstall`

to install plugins.
