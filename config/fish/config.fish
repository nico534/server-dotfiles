set fish_greeting ""
function setenv; 
  if [ $argv[1] = PATH ]
      # Replace colons and spaces with newlines
      set -gx PATH (echo $argv[2] | tr ': ' \n)
  else
    set -gx $argv
  end
end
source ~/.config/env



### Functions ###

# Functions needed for !! and !$
# Will only work in default (emacs) mode.
# Will NOT work in vi mode.
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
bind ! __history_previous_command
bind '$' __history_previous_command_arguments

### Aliases ###
alias ..="cd .."
alias ...="cd ../.."

alias grep="grep --color=auto"
alias egrep="egrap --color=auto"
alias fgrep="fgrep --color=auto"

alias cp="cp -i"
alias df="df -h"
alias free="free -m"

alias psmem="ps auxf | sort -nr -k 4"
alias psmem10="ps auxf | sort -nr -k 4 | head -10"

alias pscpu="ps auxf | sort -nr -k 3"
alias pscpu10="ps auxf | sprt -nr -k 3 | head -10"

alias addup="git add -u"
alias addall="git add ."
alias checkout="git checkout"
alias commit="git commit -m"
alias pull="git pull origin"
alias push="git push origin"
alias gstat="git status"

alias update-configs="~/.config/scripts/updateConfigs.sh"
alias rem="gio trash"

alias vim="nvim"
alias grepClass="xprop | grep CLASS"
alias unmountDisk="udisksctl unmount -b"
alias removeDisk="udisksctl power-off -b"

alias stopdocker="docker stop (docker ps -a -q)"
alias remdocker="docker rm (docker ps -a -q)"

