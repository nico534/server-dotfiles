call plug#begin('~/.local/share/nvim/plugged')

"> General
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mrk21/yaml-vim'
Plug 'elzr/vim-json'
Plug 'rhysd/vim-grammarous'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'
"Plug 'tpope/vim-commentary'
Plug 'airblade/vim-gitgutter'
Plug 'mkitt/tabline.vim'
Plug 'preservim/nerdcommenter'
Plug 'jiangmiao/auto-pairs'

"> Navigation
Plug 'terryma/vim-smooth-scroll'
Plug 'preservim/nerdtree'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
"Plug 'nvim-telescope/telescope.nvim'

"> rust
Plug 'rust-lang/rust.vim'

"> Haskell
Plug 'neovimhaskell/haskell-vim'

"> Go-Programming
Plug 'fatih/vim-go'
Plug 'SirVer/ultisnips'

"> Theme
Plug 'NLKNguyen/papercolor-theme'
call plug#end()

colorscheme PaperColor

" nerdtree Settings
map <C-n> :NERDTreeToggle<CR>

" run prettier
map <C-p> :CocCommand prettier.formatFile<CR>

" Vim-Smooth-Scroll Strokes
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 5, 2)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 5, 2)<CR>

" Basic settings
syntax on
filetype plugin indent on
set clipboard=unnamedplus
set encoding=utf-8
set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set number relativenumber
set background=dark
"set termguicolors

set foldmethod=marker foldmarker={,} foldlevel=1

" Spellcheck
set spelllang=en_us,de
nnoremap <silent> <F11> :set spell!<cr>
inoremap <silent> <F11> <C-O>:set spell!<cr>

" Autocompletion
set wildmode=longest,list,full

" Fix splitting
set splitbelow splitright


" Rust Settings
" Keybindings
map <C-f> :RustFmt<CR>

" Settings
let g:rustfmt_autosave = 1

" coc config
let g:coc_global_extensions = [
    \ 'coc-snippets',
    \ 'coc-pairs',
    \ 'coc-tsserver',
    \ 'coc-eslint',
    \ 'coc-prettier',
    \ 'coc-json',
    \ 'coc-rls',
    \ 'coc-java',
    \ 'coc-go',
    \ ]

:highlight CocFloating ctermbg=8
:highlight CocErrorFloat ctermfg=15
