#!/bin/bash
DOTFILE_DIR=$HOME/.dotfiles
CONFIG_DIR=$DOTFILE_DIR/config
HOME_CONFIG_DIR=$DOTFILE_DIR/home_config

echo "Creating symlinks to .config"
for dir in "$CONFIG_DIR"/*
do
    dir=${dir%*/}
    foundFile=${dir##*/}
    echo "$foundFile"
    rm $HOME/.config/$foundFile
    ln -s $CONFIG_DIR/$foundFile $HOME/.config/
done

#echo "Creating symlinks to home-folder"
#for dir in "$HOME_CONFIG_DIR"/*
#do
#    dir=${dir%*/}
#    foundFile=${dir##*/}
#    echo "$foundFile"
#    rm $HOME/.$foundFile
#    ln -s $HOME_CONFIG_DIR/$foundFile $HOME/.$foundFile
#done
